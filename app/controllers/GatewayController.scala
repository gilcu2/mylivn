package controllers

import javax.inject._

import models.{Comment, Person, Post}
import play.api.{Configuration, Logger}
import play.api.mvc._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, Promise, TimeoutException}
import play.api.libs.ws.WSClient
import play.api.libs.json._
import utils.Conversions._
import play.api.http.{Status => StatusResult}

@Singleton
class GatewayController @Inject()(config: Configuration, ws: WSClient, val components: ControllerComponents)(implicit ec: ExecutionContext)
  extends AbstractController(components) {

  val backendUrl = config.get[String]("BackendUrl")
  val userUrl = backendUrl + "users/"
  val postUrl = backendUrl + "posts/"
  val commentUrl = backendUrl + "comments/"
  val callTimeout = 1000.millis

  def registerUser = Action.async(parse.json) { requestIn =>

    def call(name: String): Future[Result] = {
      val data = Json.obj("username" -> name)
      val requestOut = ws.url(userUrl).addHttpHeaders("Accept" -> "application/json").withRequestTimeout(callTimeout)
      requestOut.post(data).map {
        response =>
          val result = (response.json \ "entityKey").validate[Long]
          result match {
            case s: JsSuccess[Long] => Ok(response.json)
            case e: JsError =>
              Logger.warn(s"Bad response calling $userUrl: $e")
              BadGateway(JsError.toJson(e))
          }

      }.recover {
        case e: TimeoutException =>
          Logger.warn(s"Timeout calling POST $userUrl")
          GatewayTimeout(s"Timeout calling POST $userUrl")
      }
    }

    val futureAuthorization = validateAuthorization(requestIn)
    val futureName: Future[Either[String, String]] = validateUserName(requestIn)

    futureAuthorization.flatMap { eitherAuth =>
      futureName.withFilter(eitherName =>
        eitherAuth.isRight && eitherName.isRight)
        .flatMap(eitherName =>
          call(eitherName.right.get))
    }.recover { case _ => BadRequest(Json.toJson("Not authorized user or bad user name")) }

  }

  def deleteUser = Action.async { requestIn =>

    def call(id: Long): Future[Result] = {
      val requestOut = ws.url(userUrl + id).withRequestTimeout(callTimeout)
      requestOut.delete
        .map(result =>
          if (result.status == StatusResult.OK) Ok(s"User $id deleted") else BadGateway(s"Problem calling DELETE $userUrl"))
        .recover {
          case e =>
            Logger.warn(s"Problem calling DELETE $userUrl: $e")
            BadGateway(s"Problem calling DELETE $userUrl: $e")
        }
    }

    val futureAuthorization = validateAuthorization(requestIn)

    futureAuthorization.withFilter(_.isRight).flatMap(eitherAuth =>
      call(eitherAuth.right.get))
      .recover { case _ => BadRequest(Json.toJson("Not authorized user")) }

  }

  def createPost = Action.async(parse.json) { requestIn =>

    def call(post: Post): Future[Result] = {
      val data = Json.toJson(post)
      val requestOut = ws.url(postUrl).addHttpHeaders("Accept" -> "application/json").withRequestTimeout(callTimeout)
      requestOut.post(data).map {
        response =>
          val result = (response.json \ "entityKey").validate[Long]
          result match {
            case s: JsSuccess[Long] => Ok(response.json)
            case e: JsError =>
              Logger.warn(s"Bad response calling $postUrl: $e")
              BadGateway(JsError.toJson(e))
          }

      }.recover {
        case e =>
          Logger.warn(s"Problem calling POST $userUrl: $e")
          GatewayTimeout(s"Problem calling POST $userUrl: $e")
      }
    }

    val futureAuthorization = validateAuthorization(requestIn)
    val futurePost = validatePost(requestIn)

    futureAuthorization.flatMap { eitherAuth =>
      futurePost.withFilter(eitherPost =>
        eitherAuth.isRight && eitherPost.isRight &&
          eitherPost.right.get.userKey == eitherAuth.right.get)
        .flatMap(eitherPost => call(eitherPost.right.get))
    }.recover { case _ => BadRequest(Json.toJson("Not authorized user or bad Post")) }

  }

  def deletePost(postId: Long) = Action.async { requestIn =>

    def call(postId: Long): Future[Result] = {
      val requestOut = ws.url(s"$postUrl$postId").withRequestTimeout(callTimeout)
      requestOut.delete.map {
        response => if (response.status == StatusResult.OK) Ok(s"Post $postId deleted") else BadGateway(s"Problem calling DELETE $userUrl")
      }.recover {
        case e =>
          Logger.warn(s"Problem calling POST $userUrl: $e")
          BadGateway(s"Problem calling POST $userUrl: $e")
      }
    }

    val futureAuthorization = validateAuthorization(requestIn)
    val futureUserId = getPostUserId(postId)

    futureAuthorization.flatMap { eitherAuth =>
      futureUserId.withFilter(eitherUserId =>
        eitherAuth.isRight && eitherUserId.isRight &&
          eitherUserId.right.get == eitherAuth.right.get)
        .flatMap(eitherPost => call(postId))
    }.recover { case _ => BadRequest(Json.toJson("Not authorized user or bad request")) }

  }

  def createComment = Action.async(parse.json) { requestIn =>

    def call(comment: Comment): Future[Result] = {
      val data = Json.toJson(comment)
      val requestOut = ws.url(commentUrl).addHttpHeaders("Accept" -> "application/json").withRequestTimeout(callTimeout)
      requestOut.post(data).map {
        response =>
          val result = (response.json \ "entityKey").validate[Long]
          result match {
            case s: JsSuccess[Long] => Ok(response.json)
            case e: JsError =>
              Logger.warn(s"Bad response calling $commentUrl: $e")
              BadGateway(JsError.toJson(e))
          }

      }.recover {
        case e =>
          Logger.warn(s"Problem calling POST $commentUrl: $e")
          BadGateway(s"Problem calling POST $commentUrl: $e")
      }
    }

    val futureAuthorization = validateAuthorization(requestIn)
    val futureComment = validateComment(requestIn)

    futureAuthorization.flatMap { eitherAuth =>
      futureComment.withFilter(eitherComment =>
        eitherAuth.isRight && eitherComment.isRight)
        .flatMap(eitherComment => call(eitherComment.right.get))
    }.recover { case _ => BadRequest(Json.toJson("Not authorized user or bad Comment")) }

  }

  def deleteComment(commentId: Long): Action[AnyContent] = Action.async { requestIn =>

    def call(commentId: Long): Future[Result] = {
      val requestOut = ws.url(s"$commentUrl$commentId").withRequestTimeout(callTimeout)
      requestOut.delete.map {
        response =>
          if (response.status == StatusResult.OK) Ok(s"Comment $commentId deleted") else
          BadGateway(s"Problem calling DELETE $$commentUrl$commentId")
      }.recover {
        case e =>
          Logger.warn(s"Problem calling POST $userUrl: $e")
          BadGateway(s"Problem calling POST $userUrl: $e")
      }
    }

    val futureAuthorization = validateAuthorization(requestIn)
    val futureComment = validateComment4Delete(commentId,futureAuthorization)
      .recover {
          case e =>
            Logger.warn(s"Ivalid comment  $commentId: $e")
            Left(s"Ivalid comment  $commentId: $e")
        }

    futureAuthorization.flatMap { eitherUserId =>
      futureComment.withFilter(eitherComment => eitherUserId.isRight && eitherComment.isRight)
        .flatMap(eitherComment => call(commentId))
    }.recover { case _ => BadRequest(Json.toJson("Not authorized user or bad comment")) }

  }

  def getPostWithComments(postId: Long): Action[AnyContent] = Action.async { requestIn =>

    val futurePost: Future[Either[String, Post]] = getPost(postId)
    val futureComments: Future[Either[String, Seq[Comment]]] = getComments(postId)

    futurePost.flatMap { eitherPost =>
      futureComments.withFilter(eitherComments => eitherPost.isRight && eitherComments.isRight)
        .map(eitherComments => {
          val r = eitherPost.right.get.copy(comments = Some(eitherComments.right.get))
          Ok(Json.toJson(r))
        })
    }.recover { case _ => BadRequest(Json.toJson(s"Problem getting post and comments of post $postId")) }


  }

  private def validateUserName(request: Request[JsValue]) = Future {

    val posibleName: JsResult[String] = (request.body \ "username").validate[String]
    posibleName match {
      case s: JsSuccess[String] if s.get.length > 0 => Right(s.get)
      case s: JsSuccess[String] => Left("Bad input in registerUser")
      case _ => Left("Bad input in registerUser")
    }
  }

  def getUser(id: Long): Future[Either[String, Person]] = {
    implicit val personReads = Json.reads[Person]

    val request = ws.url(s"$userUrl$id").addHttpHeaders("Accept" -> "application/json").withRequestTimeout(callTimeout)
    request.get.map { response =>
      val result = response.json.validate[Person]
      result match {
        case s: JsSuccess[Person] => Right(s.get)
        case e: JsError =>
          Logger.warn(s"Bad response calling POST $userUrl: $e")
          Left(s"Problem calling $userUrl$id: $e")
      }

    }.recover {
      case e =>
        Logger.warn(s"Problem calling $userUrl$id: $e")
        Left(s"Problem calling $userUrl$id: $e")
    }
  }

  private def validatePost(request: Request[JsValue]): Future[Either[String, Post]] = Future {

    val posiblePost: JsResult[Post] = request.body.validate[Post]
    posiblePost match {
      case s: JsSuccess[Post] => Right(s.get)
      case e => Left(s"Bad input in createPost: $e")
    }
  }

  private def getPost(postKey: Long): Future[Either[String, Post]] = {
    val request = ws.url(s"$postUrl$postKey").withRequestTimeout(callTimeout)
    request.get.map { response =>
      val result = response.json.validate[Post]
      result match {
        case s: JsSuccess[Post] => Right(s.get)
        case e: JsError =>
          Logger.warn(s"Bad response calling $$postUrl$postKey: $e")
          Left(s"Bad response calling $$postUrl$postKey: $e")
      }

    }.recover {
      case e =>
        Logger.warn(s"Problem calling $postUrl$postKey: $e")
        Left(s"Problem calling $postUrl$postKey: $e")
    }

  }

  private def validateComment(request: Request[JsValue]): Future[Either[String, Comment]] = {

    val posibleComment: JsResult[Comment] = request.body.validate[Comment]
    posibleComment match {
      case s: JsSuccess[Comment] =>
        val comment = s.get
        val futurePost = getPost(comment.postKey)
        futurePost
          .withFilter(eitherPost =>
            eitherPost.isRight && eitherPost.right.get.postKey.get == comment.postKey)
          .map(x => Right(comment))
          .recover {
            case e => Left(s"Invalid comment $comment:$e")
          }
      case _ => Future(Left("Bad input in createComment"))
    }
  }

  private def getComments(postKey: Long): Future[Either[String, Seq[Comment]]] = {

    def enrichComments(comments: Seq[Comment]): Future[Seq[Comment]] = Future.sequence {

      comments.map(comment => getUser(comment.userKey)
        .map(eitherUser => if (eitherUser.isRight) comment.copy(userName = Some(eitherUser.right.get.name))
        else comment.copy(userName = Some("deleted user")))
      )
    }

    val request = ws.url(s"$commentUrl").withRequestTimeout(callTimeout)
    val futureComments: Future[Either[String, Seq[Comment]]] = request.get.map { response =>
      val result = response.json.validate[Seq[Comment]]
      result match {
        case s: JsSuccess[Seq[Comment]] => Right(s.get.filter(_.postKey == postKey))
        case e: JsError =>
          Logger.warn(s"Bad response calling $commentUrl: $e")
          Left(s"Bad response calling $commentUrl: $e")
      }

    }.recover {
      case e =>
        Logger.warn(s"Problem calling $commentUrl: $e")
        Left(s"Problem calling $commentUrl: $e")
    }

    futureComments.withFilter(_.isRight).flatMap(x => enrichComments(x.right.get).map(Right(_)))

  }

  private def validateComment4Delete(commentId: Long, futureUserId: Future[Either[String, Long]]):
  Future[Either[String, Comment]] = {


    val requestOut = ws.url(s"$commentUrl$commentId").withRequestTimeout(callTimeout)
    val futureComment: Future[Either[String, Comment]] = requestOut.get.map(response => {
      val result = response.json.validate[Comment]
      result match {
        case s: JsSuccess[Comment] => Right(s.get)
        case e: JsError => Left(s"Invalid response GET $commentUrl$commentId: $response ")
      }
    })

    val futurePost: Future[Either[String, Post]] = futureComment.withFilter(_.isRight)
      .flatMap(eitherComment=>getPost(eitherComment.right.get.postKey))
    futureComment.flatMap(eitherComment =>
      futurePost.flatMap(eitherPost =>
        futureUserId.withFilter(eitherUserId =>
          eitherComment.isRight && eitherPost.isRight && eitherUserId.isRight &&
            (eitherPost.right.get.userKey == eitherUserId.right.get || eitherComment.right.get.userKey == eitherUserId.right.get))
          .map(x => eitherComment)
          .recover {
            case e => Left(s"Invalid comment $eitherComment:$e")
          }
      ))
  }

  private def validateAuthorization(requestIn: Request[_]): Future[Either[String, Long]] = {

    val optStringId = requestIn.headers.get("x-auth-token")
    optStringId match {
      case Some(idString) =>
        val optId = toLong(idString)
        if (optId.nonEmpty) {
          val request = ws.url(s"$userUrl${optId.get}").addHttpHeaders("Accept" -> "application/json").withRequestTimeout(callTimeout)
          request.get.map { response =>
            val result = response.json.validate[Person]
            result match {
              case s: JsSuccess[Person] => Right(optId.get)
              case e: JsError => Left(s"Invalid user id $idString")
            }

          }.recover {
            case e: TimeoutException =>
              Logger.warn(s"Timeout calling $userUrl")
              Left(s"Timeout calling $userUrl")
          }
        }
        else Future(Left(s"Invalid user id $idString"))
      case _ => Future(Left(s"Invalid user id"))

    }

  }

  private def getPostUserId(postId: Long): Future[Either[String, Long]] = {

    val request = ws.url(s"$postUrl$postId").withRequestTimeout(callTimeout)
    request.get.map { response =>
      val result = response.json.validate[Post]
      result match {
        case s: JsSuccess[Post] => Right(s.get.userKey)
        case e: JsError => Left(s"Invalid response GET $postUrl$postId: $response ")
      }

    }.recover {
      case e =>
        Logger.warn(s"Problem calling $userUrl: $e")
        Left(s"Problem calling $userUrl: $e")
    }

  }


}
