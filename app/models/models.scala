package models

import play.api.libs.json.{Format, JsValue, Json}

case class Person(userKey: Long, name: String)

object Person {
  implicit val format: Format[Person] = Json.format[Person]
}

case class Post(postKey: Option[Long], userKey: Long, title: String, description: String, comments: Option[Seq[Comment]])

object Post {
  implicit val format: Format[Post] = Json.format[Post]
}

case class Comment(userKey: Long, postKey: Long, text: String, userName: Option[String])

object Comment {
  implicit val format: Format[Comment] = Json.format[Comment]
}