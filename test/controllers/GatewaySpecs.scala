package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneServerPerSuite
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._

class BackendCalls(ws: WSClient, baseUrl: String) {

  // curl -H "Content-Type: application/json" -X POST -d '{"username":"Pedro"}'  http://localhost:9999/api/v0/users
  def registerUser(name: String) = {
    val data = Json.obj("username" -> name)
    val url = baseUrl + "/api/v0/users"
    val request = ws.url(url).addHttpHeaders("Content-Type" -> "application/json")
    request.post(data)
  }

}

class GatewayCalls(ws: WSClient, baseUrl: String) {

  // curl -H "Content-Type: application/json" -X POST -d '{"username":"Juan"}' -H "x-auth-token: 8" http://localhost:9000/api/v0/users
  def registerUser(name: String, authUserId: Long) = {
    val data = Json.obj("username" -> name)
    val url = baseUrl + "/api/v0/users"
    val request = ws.url(url).addHttpHeaders("Content-Type" -> "application/json").addHttpHeaders("x-auth-token" -> authUserId.toString)
    request.post(data)
  }

  // curl -H "Content-Type: application/json" -X POST -d '{"username":"Juan"}' -H "x-auth-token: 8" http://localhost:9000/api/v0/users
  def deleteUser(id: Long) = {
    val url = baseUrl + "/api/v0/users"
    val request = ws.url(url).addHttpHeaders("x-auth-token" -> id.toString)
    request.delete()
  }

  // curl -H "Content-Type: application/json" -X POST -d '{"userKey":11,"title": "title","description": "description"}'
  // -H "x-auth-token: 11" http://localhost:9000/api/v0/posts
  def createPost(title: String, description: String, authUserId: Long) = {
    val data = Json.obj("userKey" -> authUserId, "title" -> title, "description" -> description)
    val url = baseUrl + "/api/v0/posts"
    val request = ws.url(url).addHttpHeaders("Content-Type" -> "application/json").addHttpHeaders("x-auth-token" -> authUserId.toString)
    request.post(data)
  }

  // curl  -X DELETE -H "x-auth-token: 10" http://localhost:9000/api/v0/posts/3
  def deletePost(id: Long, authUserId: Long) = {
    val url = baseUrl + "/api/v0/posts/" + id
    val request = ws.url(url).addHttpHeaders("x-auth-token" -> authUserId.toString)
    request.delete()
  }

  // curl  http://localhost:9000/api/v0/posts/1
  def getPost(id: Long) = {
    val url = baseUrl + "/api/v0/posts/" + id
    val request = ws.url(url)
    request.get()
  }


  // curl -H "Content-Type: application/json" -X POST -d '{"userKey":12,"postKey": 1,"text": "comment text"}'
  // -H "x-auth-token: 12" http://localhost:9000/api/v0/comments
  def createComment(postKey: Long, text: String, authUserId: Long) = {
    val data = Json.obj("userKey" -> authUserId, "postKey" -> postKey, "text" -> text)
    val url = baseUrl + "/api/v0/comments"
    val request = ws.url(url).addHttpHeaders("Content-Type" -> "application/json").addHttpHeaders("x-auth-token" -> authUserId.toString)
    request.post(data)
  }

  // curl  -X DELETE -H "x-auth-token: 10" http://localhost:9000/api/v0/posts/3
  def deleteComment(id: Long, authUserId: Long) = {
    val url = baseUrl + "/api/v0/posts/" + id
    val request = ws.url(url).addHttpHeaders("x-auth-token" -> authUserId.toString)
    request.delete()
  }

}

class GatwaySpecs extends PlaySpec with GuiceOneServerPerSuite {

  val wsClient = app.injector.instanceOf[WSClient]
  val bkAddress = s"http://localhost:9999"
  val bkCalls = new BackendCalls(wsClient, bkAddress)
  val gwAddress = s"http://localhost:$port"
  val gwCalls = new GatewayCalls(wsClient, gwAddress)
  val json = await(bkCalls.registerUser("Juan")).json
  val authorisedId = (json \ "entityKey").validate[Long].asOpt.get

  "Users must be created" in {

    val response = await(gwCalls.registerUser("Pedro", authorisedId))

    response.status mustBe OK
  }

  "Users must be created and deleted" in {

    val responseCreate = await(gwCalls.registerUser("Ivan", authorisedId))
    val ivanId = (responseCreate.json \ "entityKey").validate[Long].asOpt.get
    val responseDelete = await(gwCalls.deleteUser(ivanId))

    responseDelete.status mustBe OK
  }

  "Post must be created" in {

    val response = await(gwCalls.createPost("testTitle", "testDescription", authorisedId))

    response.status mustBe OK
  }

  "Post must be created and deleted" in {

    val responseCreate = await(gwCalls.createPost("testTitle", "testDescription", authorisedId))
    val postId = (responseCreate.json \ "entityKey").validate[Long].asOpt.get
    val responseDelete = await(gwCalls.deletePost(postId, authorisedId))

    responseDelete.status mustBe OK
  }


  "Comment must be created" in {

    val responsePost = await(gwCalls.createPost("testTitle", "testDescription", authorisedId))
    val postId = (responsePost.json \ "entityKey").validate[Long].asOpt.get

    val response = await(gwCalls.createComment(postId, "Comment text", authorisedId))

    response.status mustBe OK
  }

  "Comment must be created and deleted" in {

    // await is from play.api.test.FutureAwaits
    val responsePost = await(gwCalls.createPost("testTitle", "testDescription", authorisedId))
    val postId = (responsePost.json \ "entityKey").validate[Long].asOpt.get

    val responseComment = await(gwCalls.createComment(postId, "Comment text", authorisedId))
    val commentId = (responsePost.json \ "entityKey").validate[Long].asOpt.get

    val responseDelete = await(gwCalls.deleteComment(commentId, authorisedId))

    responseDelete.status mustBe OK
  }

  "Post must be get with comments and author comments" in {

    // await is from play.api.test.FutureAwaits
    val responsePost = await(gwCalls.createPost("testTitle", "testDescription", authorisedId))
    val postId = (responsePost.json \ "entityKey").validate[Long].asOpt.get

    val responseComment = await(gwCalls.createComment(postId, "Comment text", authorisedId))
    val commentId = (responsePost.json \ "entityKey").validate[Long].asOpt.get

    val response = await(gwCalls.getPost(commentId))
    val commentAuthorName = (response.json \\ "userName") (0).validate[String].asOpt.get

    response.status mustBe OK
  }



}
